# PILOT RC Basestation Client

Download and install the latest [release](https://github.com/griffinpuc/pilotcontroller/releases) <br />
View the [wiki](https://github.com/griffinpuc/pilotcontroller/wiki) for help and information
<br />
<br />
## About this Software ##
PILOT Basestation client provides the PILOT RC hardware with additional functionality. Stream live telemetry data to the basestation client to view real-time flight statistics and diagnostics as well as providing flight mapping tools. 
<br />
<br />
## Software Requirements ##
- Available USB Connection<br />
- PILOT RC Arduino Module (Available for purchase [here](https://www.schindlerelectronics.com/store))
<br />
<br />

## Additional Resources ##
[PILOT RC Hardware Page](https://www.schindlerelectronics.com/)
