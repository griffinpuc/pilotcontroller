﻿using Microsoft.Maps.MapControl.WPF;
using PilotClientGUI;
using PilotControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PilotControl
{
    /// <summary>
    /// Interaction logic for TestingWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal static MainWindow main;
        Dictionary<string, CancellationTokenSource> threads = new Dictionary<string, CancellationTokenSource>();

        public MainWindow()
        {
            main = this;
            InitializeComponent();
            initializeCOMS();

        }

        public void showVersion_Click(object sender, RoutedEventArgs e)
        {
            VersionPopup vp = new VersionPopup();
            vp.Show();
        }

        public void showGithub_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.github.com/griffinpuc/pilotcontroller");
        }

        public void initializeCOMS()
        {
            string[] ports = SerialMonitor.retrievePorts();

            foreach(string port in ports)
            {
                MenuItem item = new MenuItem { Header=port, Name=port, IsCheckable=true };
                item.Click += com_Click;
                COMmenu.Items.Add(item);
            }


        }

        public void startMonitor(string COMPORT)
        {
            SerialMonitor smon = new SerialMonitor(COMPORT);
            CancellationTokenSource cts = new CancellationTokenSource();

            ThreadPool.QueueUserWorkItem(new WaitCallback(smon.MonitorCOM), cts.Token);
            threads.Add(COMPORT, cts);
        }

        public void endMonitor(string COMPORT)
        {
            CancellationTokenSource cts = threads.Where(pair => pair.Key.Equals(COMPORT)).Select(pair => pair.Value).FirstOrDefault();
            try
            {
                cts.Cancel();
                Thread.Sleep(1500);
                cts.Dispose();
                threads.Remove(COMPORT);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        public void setMap(double lat, double lng)
        {
            Dispatcher.Invoke(new Action(() => { myMap.Center = new Location(lat, lng); }));
        }

        private void com_Click(object sender, RoutedEventArgs e)
        {
            var item = sender as MenuItem;
            if (item.IsChecked)
            {
                item.IsChecked = true;
                string COMPORT = item.Name;
                startMonitor(COMPORT);
            }
            else
            {
                item.IsChecked = false;
                string COMPORT = item.Name;
                endMonitor(COMPORT);
            }
          

        }


        internal string Battery
        {
            get { return battery_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { battery_label.Content = value; })); }
        }
        internal string Roll
        {
            get { return roll_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { roll_label.Content = value; })); }
        }
        internal string Pitch
        {
            get { return pitch_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { pitch_label.Content = value; })); }
        }
        internal string Yaw
        {
            get { return yaw_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { yaw_label.Content = value; })); }
        }
        internal string Latitude
        {
            get { return lat_label.Content.ToString(); }
            set { 
                Dispatcher.Invoke(new Action(() => { lat_label.Content = value;
                Dispatcher.Invoke(new Action(() => { lat_label_map.Content = value; }));
            })); }
        }
        internal string Longitude
        {
            get { return lng_label.Content.ToString(); }
            set {
                Dispatcher.Invoke(new Action(() => { lng_label.Content = value;
                Dispatcher.Invoke(new Action(() => { lng_label_map.Content = value; }));
            })); }
        }
        internal string Altitude
        {
            get { return alt_label.Content.ToString(); }
            set {
                Dispatcher.Invoke(new Action(() => { alt_label.Content = value;
                Dispatcher.Invoke(new Action(() => { alt_label_map.Content = value; }));
            })); }
        }
        internal string Velocity
        {
            get { return vel_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { vel_label.Content = value; })); }
        }
        internal string Acceleration
        {
            get { return accl_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { accl_label.Content =  value; })); }
        }
        internal string Current
        {
            get { return crrnt_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { crrnt_label.Content = value; })); }
        }
    }
}
