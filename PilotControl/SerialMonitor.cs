﻿using PilotControl;
using System;
using System.IO.Ports;
using System.Threading;
namespace PilotClientGUI
{
    class SerialMonitor
    {
        static SerialPort _serialPort;
        bool cancel = false;

        public SerialMonitor(string COMPORT)
        {
            _serialPort = new SerialPort();
            _serialPort.PortName = COMPORT;
            _serialPort.BaudRate = 115200;
            _serialPort.Open();
        }

        public static string[] retrievePorts()
        {
            return SerialPort.GetPortNames();
        }


        public void MonitorCOM(object obj)
        {
            string a = _serialPort.ReadExisting();
            Telemetry newTelem = new Telemetry().parseTelemetricString(a);
            MainWindow.main.setMap(newTelem.latitude, newTelem.longitude);

            CancellationToken token = (CancellationToken)obj;

            while (true)
            {
                if (!token.IsCancellationRequested)
                {
                    a = _serialPort.ReadExisting();

                    newTelem = new Telemetry().parseTelemetricString(a);
                    if (!newTelem.battery.Equals("0"))
                    {
                        MainWindow.main.Battery = newTelem.roll.ToString();
                        MainWindow.main.Roll = newTelem.roll.ToString();
                        MainWindow.main.Pitch = newTelem.pitch.ToString();
                        MainWindow.main.Yaw = newTelem.yaw.ToString();
                        MainWindow.main.Latitude = newTelem.latitude.ToString();
                        MainWindow.main.Longitude = newTelem.longitude.ToString();
                        MainWindow.main.Altitude = newTelem.altitude.ToString();
                        MainWindow.main.Velocity = newTelem.velocity.ToString();
                        MainWindow.main.Acceleration = newTelem.acceleration.ToString();
                        MainWindow.main.Current = newTelem.current.ToString();

                        Thread.Sleep(200);
                    }

                }
                else
                {
                    _serialPort.Close();
                    break;
                }

            }

        }

        public void LogCOM()
        {

            string logs = "";

            for (int i = 0; i < 100; i++)
            {
                string a = _serialPort.ReadExisting();

                Telemetry newTelem = new Telemetry().parseTelemetricString(a);
                logs += newTelem.returnPrintout();


                Thread.Sleep(200);
                Console.WriteLine(i);
            }

            System.IO.File.WriteAllText(@"D:\Workspace\PILOT\dump\log.txt", logs);

        }
    }
}