﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PilotControl
{
    class Telemetry
    {

        public double battery { get; set; }

        public double roll { get; set; }
        public double pitch { get; set; }
        public double yaw { get; set; }

        public double latitude { get; set; }
        public double longitude { get; set; }
        public double altitude { get; set; }

        public double velocity { get; set; }
        public double acceleration { get; set; }
        public double current { get; set; }

        public Telemetry parseTelemetricString(string datastring)
        {
            string sep = "\t";
            string[] datas = datastring.Split(sep.ToCharArray());

            try
            {
                this.battery = Double.Parse(datas[0]);
                this.roll = Double.Parse(datas[1]);
                this.pitch = Double.Parse(datas[2]);
                this.yaw = Double.Parse(datas[3]);
                this.latitude = Double.Parse(datas[4]);
                this.longitude = Double.Parse(datas[5]);
                this.altitude = Double.Parse(datas[6]);
                this.velocity = Double.Parse(datas[7]);
                this.acceleration = Double.Parse(datas[8]);
                this.current = Double.Parse(datas[9]);
            }
            catch(Exception e)
            {

            }


            return this;
        }

        public string returnPrintout()
        {
            string sp = "    ";
            return(System.DateTime.Now + ": " + battery + sp + roll + sp + pitch + sp + yaw + sp + latitude + sp + longitude + sp + altitude + sp +velocity + sp + acceleration + sp + current + "\n");
        }

    }
}
