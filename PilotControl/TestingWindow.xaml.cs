﻿using PilotClientGUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PilotControl
{
    /// <summary>
    /// Interaction logic for TestingWindow.xaml
    /// </summary>
    public partial class TestingWindow : Window
    {
        internal static TestingWindow main;

        public TestingWindow()
        {
            main = this;
            InitializeComponent();

            SerialMonitor smon = new SerialMonitor();
            Thread comThread = new Thread(new ThreadStart(smon.MonitorCOM));
            comThread.Start();
        }

        
        internal string Battery
        {
            get { return battery_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { battery_label.Content = "BATTERY: " + value; })); }
        }
        internal string Roll
        {
            get { return roll_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { roll_label.Content = "ROLL: " + value; })); }
        }
        internal string Pitch
        {
            get { return pitch_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { pitch_label.Content = "PITCH: " + value; })); }
        }
        internal string Yaw
        {
            get { return yaw_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { yaw_label.Content = "YAW: " + value; })); }
        }
        internal string Latitude
        {
            get { return lat_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lat_label.Content = "LATITUDE: " + value; })); }
        }
        internal string Longitude
        {
            get { return lng_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { lng_label.Content = "LONGITUTDE: " + value; })); }
        }
        internal string Altitude
        {
            get { return alt_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { alt_label.Content = "ALTITUDE: " + value; })); }
        }
        internal string Velocity
        {
            get { return vel_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { vel_label.Content = "VELOCITY: " + value; })); }
        }
        internal string Acceleration
        {
            get { return accl_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { accl_label.Content = "ACCELERATION: " + value; })); }
        }
        internal string Current
        {
            get { return crrnt_label.Content.ToString(); }
            set { Dispatcher.Invoke(new Action(() => { crrnt_label.Content = "CURRENT: " + value; })); }
        }
    }
}
